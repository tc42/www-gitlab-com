---
layout: markdown_page
title: "Product Marketing"
---
# Welcome to the Product Marketing Handbook  

The Product Marketing organization includes Product Marketing, Content Marketing, and Partner/Channel Product Marketing.

[Up one level to the Marketing Handbook](/handbook/marketing/)

## On this page

* [Key responsibilities](#responsibilities)
* [Priority focus areas](#priority-focus-areas)
* [Release management](#release-management)
* [Sales enablement](#sales-enablement)
* [Customer case study creation](#case-study)


## Product Marketing Handbooks  
* [Content marketing](/handbook/marketing/product-marketing/content-marketing/)
* [Partner marketing](/handbook/marketing/product-marketing/partner-marketing/)
* [Reseller marketing](/handbook/marketing/product-marketing/reseller-marketing/)

## Key Responsibilities<a name="responsibilities"></a>

At the highest level, the goal of product marketing is to communicate the value of our product
or services to our target audience.

Product marketing has three primary responsibilities.

1. **Analysis**: Product marketing is responsible for researching and analyzing the market, competition, product, and customer to find unique insights that aid the sales process.
2. **Messaging**: Product marketing is responsible for defining and managing the value proposition, messaging, and positioning of new and existing products and features.
3. **Enablement**: Product marketing is responsible for listening to the sales teams to understand the challenges and opportunities they face in the sales process. Additionally, product marketing supports the sales team with collateral, training, and go-to-market strategy.


## Priority Focus Areas<a name="priority-focus-areas"></a>

* Customer Research 
* Sales Training
* Sales Enablement 
* Moving from GitLab CE to GitLab EE
* Website Messaging and Flow 

If you're interested in learning more about the specific deliverables or read more on the insights that were shared, take a look a the documented feedback in this presentation. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/1b59ZpkTmn5_sn8ryi-Ai8eacX4HinurAPq194bnl7Rw/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Release Management<a name="release-management"></a>

### Release Kickoff

The goal is to enable sales and marketing/PR to work one release ahead of the product release cycle. The proposed process to get us there is below.   

- After the kickoff meeting Job and Amara will agree on priority features
- A sister marketing issue will be created in the [marketing project](https://gitlab.com/gitlab-com/marketing) for each priority feature
- In the sister issue, the developer working on the feature will work to answer the following:
    - Who are we building the feature for
    - What pain will the feature solve
    - How will the user/team/admin benefit from having the feature (focus on measurable benefits)
    - How will this feature change a user/team/admins workflow (e.g. this used to be a 10-step process now it is 1-click)
    - What are the current limitations
- Prod. Marketing will answer the following
    - What is the competitive landscape
    - What are the key selling points
    - How will we message it

### Release Checklist 

The goal of our release checklist is to highlight what should be completed with each release.  

- Verify that the work for the last release was documented and issue was closed 
- Release issue using the `release` label was created in the [marketing project](https://gitlab.com/gitlab-com/marketing) 
- Priority features that were agreed upon in the release kickoff were mentioned in this release issue
- Priority features have been scheduled or demoed in GitLab University
- Marketing plan for priority features has been documented
- Priority features are fully documented with screenshots (align with technical writers)
- Priority features are shown on /features or updated in other website pages
- Priority features appear in the compare view on /features
- /direction is updated 
- Evaluate the opportunity to update existing screens or copy on pages that will be updated for the release
- Document changes made to the site in the release issue

## Sales Enablement<a name="sales-enablement"></a>

The goal of sales enablement is to empower the sales team with training, collateral, and go-to-market. 

### Sales Playbook

The sales playbook will contain a number of sales enablement materials. 

* Gitlab 25, 50, 100 word description
* GitLab pitch deck
* GitLab product breakdown sheet
* Buyer persona
* Customer stories, testimonials, and eventual case studies
* Objection handling
* CE to EE kit: 
    * CE to EE sales tear sheet (Key selling points, messaging for features, trap-setting questions, proof points) 
    * CE to EE deck  
    * Materials for CE to EE programmatic approaches (i.e. quarterly upgrade email, webcasts, etc.)
* Templated emails 
* Site content directory (i.e. links to existing site content in a simple searchable)


### GitLab University Sales and Product Training 

GLU sales and product training is a weekly meeting open to all GitLab employees to join and learn more about our industry, product, and customers.

#### **Topics**:

- New releases (selected within the release management process)
- Existing features or experiences, at the request of GitLab team members 
- Customer stories and sales process review
- Industry and competitive research 

#### **Requesting a new topic**:
 - Create an issue in the [GLU project](https://gitlab.com/gitlab-org/University)
 - Clarify questions you would like answered 
 - Offer any examples on when the topic has come up to provide additional context
 - Assign to Amara to organize and schedule

#### **GLU presentations should follow this rough structure**:
- What is the new feature or experience
- Who did we build it for
- What pain is it solving for them
- Tell us more about the customer profile and the use cases for this feature
- Walk us through how customers will use this feature (i.e. what may change in their workflow)
- What are the measurable benefits of using this feature
- What do we know about the competitive landscape
- How are we differentiated
- Are there any current limitations the sales team should be aware of
- What documentation exists today
- Summary slide: key selling points and suggested message

#### **GLU Materials**: 

* Check out the [GLU calendar](https://docs.google.com/a/gitlab.com/spreadsheets/d/18ZouDT5HASCxztDX_a8H4zHVrTN0EkqbIkuf9SpHJsM/edit?usp=sharing) to see what's scheduled. 
* GLU slides and recordings will be stored in the Sales Google Drive in the [GitLab University folder](https://drive.google.com/a/gitlab.com/folderview?id=0B41DBToSSIG_NlNFLUEwQ2JHSVk&usp=sharing).
* All recordings that do not contain senstive information will also be shared in Gitlab's public [YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg). 


### Customer Case Study Creation<a name="case-study"></a>

#### **Objectives**:
- Illustrate successful and measurable improvements in the customer’s business   
- Show other users how customers are using GitLab 
- Align content so that GitLab solutions reflect the challenges our customers, prospects and the market requirements
- Develop and grow organisational relationships: speak at events on our behalf, promote their business
- Support our value proposition - Faster from idea to production

#### **Case Study Format**:

*Headline:* The name of the customer and the benefit they gained from implementing our solution
*Sidebar Summary:* Summarize key points 
- Customer Name and Logo
- Customer Details: Country, Website
- Organisation Type - Public/Private & Industry
- Annual Revenue - If publicly available
- Employees
- Summary of Key Benefits
*The Customer:* A brief introduction of the customer, not the solution.
*The Challenge:* What was the customer trying to change or improve. What regulations or market conditions drove the customer to search for a new solution. Share the steps the customer took to solve the problem, including other products and services they investigated. 
*The Solution:* Detail the solution and how it aligns to the customers requirements. Also detail other solutions that GitLab interfaces with. Also include customer quote.
*The Results:* Detail how GitLab supported the customer to solve their problems. This is where the use of benchmarking metrics such as such as time saved, reduced costs, increased performance etc. are required. 
*About GitLab:* Short paragraph on GitlLab - about, solutions etc. Call to action of solutions offered.

*Possible Additional Supporting Documents:* 
- Customer Deal Summary – High Level PowerPoint summary after deal first signed.
- Customer Success Overview
- Infographic – Single page A4 summary with diagrams and measureable benchmarks 
- Benchmark Metrics
- Publish on website
- Video - Short video summary if customer is willing to participate - Perforce example
- Blog Post - Blog post to launch customer case study
- Keywords for SEO etc.