---
layout: markdown_page
title: GitHost Administration
category: GitHost
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## GitHost Administration

### Requesting Access

Ask any of the administrators to give you access to the GitHost UI. Within the UI you will be able to see customer instances, their setup, and their IDs.

You will need to request SSH access to the GitHost production box. For this, please send a chat message to an admin with your Public Key and ask to be added to the authorized_keys of the githost user.

### Documentation
The latest GitHost Administration docs can be found [here](https://dev.gitlab.org/gitlab/GitHost/blob/master/doc/README.md).
