---
layout: job_page
title: "Senior Backend Developer, CI/CD Pipelines"
---

CI Backend Developers are primarily tasked with improving the Continuous
Integration (CI) and Continuous Deployment (CD) functionality in GitLab. See
the description on the [CI team](/handbook/backend#ci) for more details. Senior CI
Backend Developers report to the [CI Lead](/jobs/ci-lead/).

We are currently looking for senior engineers who are proficient in the
Go programming language.

## Responsibilities

* Develop CI/CD features from proposal to polished end result.
* Support and collaborate with our [service engineers](/jobs/service-engineer) in getting to the bottom of user-reported issues and come up with robust solutions.
* Engage with the core team and the open source community to collaborate on improving GitLab.
* Manage and review code contributed by the rest of the community and work with them to get it ready for production.
* Create and maintain documentation around features and configuration to save our users time.
* Take initiative in improving the software in small or large ways to address pain points in your own experience as a developer.
* Keep code easy to maintain and keep it easy for others to contribute code to GitLab.
* Qualify developers for hiring.

## Requirements

* You can reason about software, algorithms, and performance from a high level
* You are passionate about open source
* You have worked on a production-level application using Go.
* Strong understanding of operating systems and concurrent programming
* Strong written communication skills
* Experience with Kubernetes, Docker, Nginx, Ruby/Rails, and Linux system administration a plus
* Experience with online community development a plus
* Self-motivated and have strong organizational skills
* You share our [values](/handbook/#values), and work in accordance with those values.
* [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)
