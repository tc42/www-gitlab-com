---
layout: job_page
title: "Service Engineer"
---
The rising popularity of GitLab means that our professional services are in high demand.
If you have the skills to help our clients we would love to talk to you.  

We are looking for skilled people around the world. If you would love to
work from home and help GitLab grow, this is the right spot for you.

## Responsibilities

* Engage with our customers—anything from a small advertising firm or a university, to Fortune 100 clients and
help them with anything from a simple support ticket to a customer training
* Triage customer issues and find workarounds if possible
* Engage with the development team to escalate bugs, problems, or missing information
* Communicate via email and video conferencing with potential and current clients
* Participate in the rotating "on-call" list to provide 24/7 emergency response availability
* Ensure that everything we learn from running GitLab.com is set as default or communicated to our users
* Maintain documentation relating to troubleshooting and other knowledge gained via customer interactions
* Prepare and provide customer training, and make the training materials widely available
* Submit and comment on feature requests based on customer interactions

More information can be found on the [support page in the handbook](https://about.gitlab.com/handbook/support/).

### Junior Service Engineer

* Junior Service Engineer's responsibilities cover all the support channels that are
supported by a level 3 or 4 SLA, per the definitions on the [Support](/handbook/support/#sla) page.
* Contribute with documentation
* Manage Feature Proposals created through their user interactions
* Manage Bug reports created through their user interactions
* Technical Skills
    * Able to triage basic GitLab issues
    * Able to perform basic UNIX/GitLab system administration
    * Able to solve customer tickets in a timely fashion
* Communication
    * Able to communicate clearly with customers on technical topics
    * Able to escalate issues to the right people
    * Keeps issues up-to-date with progress
    * Follows up with outstanding issues
    * Documents relatively simple answers in issues and GitLab documentation
    * Makes customers happy

### Service Engineer

* All Junior Service Engineer responsibilities with less time invested on SLA 3 and SLA 4
* Participate in the On-call schedule
* Work on SLA 1 emergency and security tickets
* Work on SLA 2 customer tickets
* Technical Skills
    * Able to triage intermediate GitLab issues and resolve them
    * Able to perform intermediate UNIX/GitLab system administration
    * Can identify relatively challenging bugs/issues with GitLab
    * Can submit merge requests for basic GitLab bugs/problems
* Workflow
    * Reliably answers on-call emergencies
    * Exceeds SLAs consistently
* Communication
    * Documents relatively challenging answers in issues and GitLab documentation
    * Helps ensure that tickets are closed in a timely manner
    * Contributes with training material/sessions

### Senior Service Engineer

* All Service Engineer responsibilities
* Able to cover all channels
* Help train new service engineers
* Improve the support workflow (ZenDesk)
* Contribute to complementary projects (e.g. GitLab University, etc.)
* Maintain good ticket performance
* Write blog articles
* Take ownership of documentation and feature requests that is based on customer interactions
* Improve the support process (on-boarding, training, escalation, etc)
* Technical Skills
    * Have deep knowledge of GitLab internals and a variety of possible configurations
    * Able to perform complex UNIX/GitLab system administration
    * Can debug challenging bugs with GitLab
    * Can submit merge requests for advanced GitLab bugs/problems
* Leadership
    * Help hire and train new Service Engineers
    * Are a go-to person for the other Service Engineers when they face tough challenges
* Communication      
    * Take ownership of documentation
    * Drives feature requests based on customer interactions  

### Staff Engineer

* All Senior Service Engineer responsibilities
* Help hire service engineers as part of the interview process
* Good ticket performance
* Good ticket satisfaction
* Maintains complementary project
* Contributes to GitLab regularly
* Technical Skills
    * Can solve most support tickets in a reasonable time without escalating to development
    * Specializes in multiple topics (e.g. LDAP, Jenkins/CI integration, Geo)
    * Should submit or review MRs with development for customer GitLab bugs/issues


## Requirements for Applicants

* Above average knowledge of Unix and Unix based Operating Systems
* Vast experience with Ruby on Rails Applications and git
* Affinity for (and experience with) providing customer support
* Excellent spoken and written English
* You share our [values](/handbook/#values), and work in accordance with those values
* A technical interview is part of the hiring process for this position.
* A customer scenario interview is part of the hiring process for this position.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first technical interview with a Service Engineer
* Candidates will then be invited to schedule a customer service interview with a Service Engineer
* Candidates will be invited to schedule a third interview with our Interim Support Lead
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
